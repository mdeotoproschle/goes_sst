#!/usr/bin/env python
import os
import xarray as xr
import numpy as np

"""
Regional Mask generator in function of ABI SST data download
"""
#==============================================================================#
# The math for this function was taken from
# https://makersportal.com/blog/2018/11/25/goes-r-satellite-latitude-and-longitude-grid-projection-algorithm

def calc_latlon(ds) :
  x = ds.x
  y = ds.y
  goes_imager_projection = ds.goes_imager_projection
  x,y = np.meshgrid(x,y)
  r_eq = goes_imager_projection.attrs["semi_major_axis"]
  r_pol = goes_imager_projection.attrs["semi_minor_axis"]
  l_0 = goes_imager_projection.attrs["longitude_of_projection_origin"] * (np.pi/180)
  h_sat = goes_imager_projection.attrs["perspective_point_height"]
  H = r_eq + h_sat
  a = np.sin(x)**2 + (np.cos(x)**2 * (np.cos(y)**2 + (r_eq**2 / r_pol**2) * np.sin(y)**2))
  b = -2 * H * np.cos(x) * np.cos(y)
  c = H**2 - r_eq**2
  r_s = (-b - np.sqrt(b**2 - 4*a*c))/(2*a)
  s_x = r_s * np.cos(x) * np.cos(y)
  s_y = -r_s * np.sin(x)
  s_z = r_s * np.cos(x) * np.sin(y)
  lat = np.arctan((r_eq**2 / r_pol**2) * (s_z / np.sqrt((H-s_x)**2 +s_y**2))) * (180/np.pi)
  lon = (l_0 - np.arctan(s_y / (H-s_x))) * (180/np.pi)
  ds = ds.assign_coords({"lat":(["y","x"],lat),"lon":(["y","x"],lon)})
  ds.lat.attrs["units"] = "degrees_north"
  ds.lon.attrs["units"] = "degrees_east"
  return ds

# Define function to get x/y bounds given lat/lon bounds from ABI sensor
def get_xy_from_latlon(ds, lats, lons) :
  lat1, lat2 = lats
  lon1, lon2 = lons
  lat = ds.lat.data
  lon = ds.lon.data
  x = ds.x.data
  y = ds.y.data
  x,y = np.meshgrid(x,y)
  x = x[(lat >= lat1) & (lat <= lat2) & (lon >= lon1) & (lon <= lon2)]
  y = y[(lat >= lat1) & (lat <= lat2) & (lon >= lon1) & (lon <= lon2)]
  return ((min(x), max(x)), (min(y), max(y)))

# ---------------------------------------------------------------------------- #
# Set folders
dirfile = f"{os.environ['OUTDIR']}"
datadir = f"{os.environ['DATADIR']}"

# Example file. Need only one time to make the mask file
ds = xr.open_dataset(f"{dirfile}/tmp_mask/ABI_GOES16.20200201T000000.L2.CSKY.nc")

# Region of Interest (ROI). San Matias Gulf
lonMin, lonMax, latMin, latMax = -66.0, -62.0, -43.0, -40.0
# Sections for trimming region
ds            = calc_latlon(ds)
lats_subset   = (latMin, latMax)
lons_subset   = (lonMin, lonMax)
((x1,x2), (y1, y2)) = get_xy_from_latlon(ds, lats_subset, lons_subset)

# Cutting ABI dataset over ROI
dx = slice(x1, x2)
dy = slice(y2, y1)
ds = ds.sel(x=dx, y=dy)

# Mask generator
ds = ds.rename({"DQF":"lsm"})
mask = (ds.lsm!=3) * 1

# Saving
mask.to_netcdf(f"{datadir}/landsea_mask_example.nc")
