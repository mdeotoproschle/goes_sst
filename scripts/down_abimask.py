#!/usr/bin/env python
import os, glob, s3fs, sys
import numpy as np
import xarray as xr
import pandas as pd

# Download Folder
tmpdir = f"{os.environ['OUTDIR']}/tmp_mask"
os.makedirs(tmpdir, exist_ok = True)
# Start time
start   = sys.argv[1]

# One month download
end    = (pd.to_datetime(start) + pd.DateOffset(months=1)).strftime("%Y%m%d")
fechas = pd.date_range(start=start, end=end, freq="3H")[:-1]

#==============================================================================#
# AMAZON repository information
# https://noaa-goes16.s3.amazonaws.com/index.html
bucket_name  = 'noaa-goes16'
product_name = 'ABI-L2-ACMF'
type_name    = 'M6_G16'
fs = s3fs.S3FileSystem(anon=True)
for fecha0 in fechas:
    # Download
    date = fecha0.round('10min')
    files = fs.glob(f"{bucket_name}/{product_name}/{date.strftime('%Y')}/{date.strftime('%j')}/{date.strftime('%H')}/OR_{product_name}-{type_name}_s{date.strftime('%Y%j%H%M')}*")
    if len(files)>0:
        fs.download(files[0],f'{tmpdir}/ABI_GOES16.{fecha0.strftime("%Y%m%dT%H%M%S")}.L2.CSKY.nc')
