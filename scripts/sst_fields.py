#!/usr/bin/env phyton

"""
Compute SST Fields during clear sky interval
"""

import os, sys
import geopandas as gpd
import shapely as shp
import numpy as np
import xarray as xr
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
from datetime import datetime

def customgrid(ax, borders, proj):
    g1 = ax.gridlines(crs=proj,draw_labels=True,linestyle='--',linewidth=1)
    g1.top_labels    = False
    g1.right_labels  = False
    g1.yformatter    = LATITUDE_FORMATTER
    g1.xformatter    = LONGITUDE_FORMATTER
    g1.xlabel_style  = {'size':12}
    g1.ylabel_style  = {'size':12}
    borders.plot(ax=ax, color='black', lw=1., zorder=2)
    return ax

# ---------------------------------------------------------------------------- #
# Set folders
dirfile = f"{os.environ['OUTDIR']}"
figdir  = f"{os.environ['FIGDIR']}"
datadir = f"{os.environ['DATADIR']}"

# ---------------------------------------------------------------------------- #
# Set lat/lon box and subset the data
lonMin, lonMax, latMin, latMax = -66.0, -62.0, -43.0, -40.0

# ---------------------------------------------------------------------------- #
# Borders
boundary_highres = cfeature.NaturalEarthFeature('physical', 'coastline', '10m') # NaturalEarthFeature
linestrings      = gpd.GeoDataFrame(geometry=[i for i in boundary_highres.geometries()])
# LineStrings borders
borders          = gpd.GeoDataFrame(geometry=gpd.GeoSeries(linestrings.geometry), crs=4326)
# Polygons borders
polygons         = gpd.GeoDataFrame(geometry=gpd.GeoSeries(shp.ops.polygonize(linestrings.geometry)), crs=4326)

#==============================================================================#
# SST ABI/GOES-16
#==============================================================================#
abisst        = xr.open_dataset(f"{dirfile}/ABI_GOES16.L2.SST.nc")
# QFLAGS
mask_abi      = abisst.DQF<=0            # good_quality_qf
abisst_subset = abisst.assign(SST=abisst["SST"]-273.16).where(mask_abi)

#==============================================================================#
# SST features
bounds = np.linspace(15,25,51)
nc     = len(bounds) - 1
cmap   = plt.cm.get_cmap('turbo', nc)
norm   = colors.BoundaryNorm(boundaries=bounds, ncolors=nc)

#==============================================================================#
# Plot
proj  = ccrs.PlateCarree()                                                    # Map Projection
times = pd.to_datetime(abisst_subset["t"].values)

for time in times:
    # Date
    abisst_filter = abisst_subset.sel(t=time)
    # Start Figure
    fig = plt.figure(figsize = (12,12))
    fig.subplots_adjust(top=0.9, bottom=0.15, left=0.01, right=0.975, wspace=0.05)
    ax  = fig.add_subplot(111, projection = proj)
    ax  = customgrid(ax, borders, proj)
    ax.set_title(f'{abisst.SST.long_name}\n {time.strftime("%Y-%m-%d %H:%M:%S")}', fontsize=15)
    fill = ax.pcolormesh(abisst_filter["lon"], abisst_filter["lat"], abisst_filter["SST"], cmap = cmap, norm=norm)
    ax.set_extent([lonMin, lonMax, latMin, latMax])
    # COLORBAR
    cb      = fig.colorbar(fill, ax=ax, orientation='horizontal', extend="both", shrink=.6)
    cb.ax.tick_params(labelsize=13)
    cb.set_ticks(bounds)
    cb.ax.locator_params(nbins=10)
    cb.ax.set_title('degree C', fontsize=15)
    #plt.show()
    plt.savefig(f"{figdir}/sst_fields_{time:%Y%m%d%H}.png", bbox_inches='tight')
    plt.close(fig)
